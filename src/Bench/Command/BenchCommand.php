<?php

namespace Bench\Command;

use Bench\Helper\ArrayHelper;
use Bench\Helper\ConfigHelper;
use Bench\Helper\FinderHelper;
use Curl\MultiCurl;
use JsonException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use ErrorException;

/**
 * Class BenchCommand
 * @package Bench\Command
 */
class BenchCommand extends Command
{
    public const VERSION = '2.1.5';

    private MultiCurl $curl;

    private int $errors = 0;

    private int $exit = 0;

    private int $total = 0;

    private int $limit;

    private array $times = [];

    public function __construct()
    {
        $this->curl = new MultiCurl();
        $this->curl->setUserAgent('PHP Bench Client/' . self::VERSION);
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('run')
            ->setDescription('Console Bench Tester')
            ->addArgument(
                'address',
                InputArgument::OPTIONAL,
                'Set the Endpoint',
                'http://localhost'
            )->addOption(
                'iteration',
                'i',
                InputOption::VALUE_REQUIRED,
                'Number of repeated requests',
                1
            )->addOption(
                'concurrency',
                'c',
                InputOption::VALUE_REQUIRED,
                'Number of concurrent requests',
                1
            )->addOption(
                'path',
                'p',
                InputOption::VALUE_REQUIRED,
                'Path to the test files',
                '.'
            )->addOption(
                'method',
                'm',
                InputOption::VALUE_REQUIRED,
                'HTTP Method',
                'GET'
            )->addOption(
                'headers',
                'H',
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'HTTP Headers',
                []
            )->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'File type',
                '*.json'
            )->addOption(
                'limit',
                'l',
                InputOption::VALUE_REQUIRED,
                'Limit in milliseconds',
                8000
            )->addOption(
                'exclude',
                'e',
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Exclude files',
                []
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ErrorException|JsonException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = microtime(true);

        $config = ConfigHelper::getConfig();

        $address     = $config->address ?? $input->getArgument('address');
        $iteration   = $config->iteration ?? $input->getOption('iteration');
        $concurrency = $config->concurrency ?? $input->getOption('concurrency');
        $method      = $config->method ?? $input->getOption('method');
        $headers     = $config->headers ?? ArrayHelper::setHeaders($input->getOption('headers'));

        $types       = $config->types ?? $input->getOption('type');
        $path        = $config->path ?? $input->getOption('path');
        $exclude     = $config->exclude ?? $input->getOption('exclude');

        $this->limit = $config->limit ?? $input->getOption('limit');

        $finder = FinderHelper::getFiles($path, $types, $exclude);

        while ($iteration > 0) {
            if (strtoupper($method) === 'GET') {
                $this->curl->addGet($address, $headers);
                $this->total++;
            }

            foreach ($finder->files()->notName('*.ini') as $file) {

                $basename = $file->getBasename('.' . $file->getExtension());
                $benchfile = sprintf('%s/%s.ini', $file->getPath(), $basename);

                $adjustedExecutionTime = $this->limit;
                if (file_exists($benchfile)) {
                    $ini = parse_ini_file($benchfile, true);

                    $expectedExecutionTime = $ini['Bench']['ExpectedExecutionTime'] ?? $this->limit;
                    $timeTolerancePercent = $ini['Bench']['TimeTolerancePercent'] ?? 0;
                    $adjustedExecutionTime = (100 + $timeTolerancePercent) / 100 * $expectedExecutionTime;
                }

                $this->curl->setConcurrency($concurrency);

                if (strtoupper($method) === 'POST') {
                    $data = $file->getContents();
                    $info = [
                        'file' => $file->getRelativePathname(),
                        'limit' => $adjustedExecutionTime ?? $this->limit,
                    ];
                    $this->curl->addPost($address, $data, $headers, $info);
                    $this->total++;
                }
            }

            $iteration--;
        }

        $output->writeln($this->getApplication()->getName() .  '@' . $this->getApplication()->getVersion() . ' by Typomedia Foundation, Philipp Speck');

        $this->curl->success(function ($instance) use ($output) {
            $time = number_format($instance->totalTime * 1000, 0, '.', '');
            $this->times[] = $time;
            $line = $instance->httpStatusCode . ' ' . $instance->url . ' ' . $time . "\t" . $instance->info['file'] . "    Expected: " . $instance->info['limit'];

            if ($time > $instance->info['limit']) {
                $this->errors++;
                $this->exit = 1;
                $output->writeln('<fg=white;bg=yellow>' . $line . '</>');
            } else {
                $output->writeln('<info>' . $line . '</info>');
            }
        });

        $this->curl->error(function ($instance) use ($output) {
            $this->errors++;
            $this->exit = 1;
            $time = number_format($instance->totalTime * 1000, 0, '.', '');
            $this->times[] = $time;
            $line = $instance->httpStatusCode . ' ' . $instance->url . ' ' . $time . "\t" . $instance->info['file'] . "    Expected: " . $instance->info['limit'];
            $output->writeln('<error>' . $line . '</error>');
        });

        $this->curl->start();
        $this->curl->close();

        $end = microtime(true);
        $time = round(($end - $start) * 1000);

        if ($this->errors > 0) {
            $output->writeln('<error>Errors: ' . $this->errors . '</error>');
        }

        $output->writeln('Executed tests: ' . $this->total);
        $output->writeln('Execution time: ' . $time . ' ms');

        if ($this->total > 0) {
            $output->writeln('Average time: ' . $this->averageTime($this->times) . ' ms');
        }

        return $this->exit;
    }

    /**
     * @param $times
     * @return float
     */
    private function averageTime($times)
    {
        $total = 0;
        foreach ($times as $time) {
            $total += $time;
        }
        return round($total / count($times));
    }
}
