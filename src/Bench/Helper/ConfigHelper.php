<?php

namespace Bench\Helper;

use JsonException;
use stdClass;

/**
 * Class ConfigHelper
 * @package Bench\Helper
 */
class ConfigHelper
{
    /**
     * @param string $file
     * @return stdClass
     * @throws JsonException
     */
    public static function getConfig(string $file = 'bench.json')
    {
        $config = new stdClass();

        if (file_exists($file)) {
            $content = file_get_contents($file) ?: '{}';
            $config = json_decode($content, false, 512, JSON_THROW_ON_ERROR);
        }

        return $config;
    }
}
