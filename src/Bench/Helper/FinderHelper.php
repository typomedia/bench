<?php

namespace Bench\Helper;

use Symfony\Component\Finder\Finder;

/**
 * Class FinderHelper
 * @package Bench\Helper
 */
class FinderHelper
{
    /**
     * @param string $path
     * @param string $type
     * @param array $exclude
     * @return Finder
     */
    public static function getFiles(string $path, string $type, array $exclude = []): Finder
    {
        $finder = new Finder();
        if (is_file($path)) {
            $finder->files()->in(dirname($path))->name(basename($path));
        } else {
            $type = '*.' . ltrim($type, '.*');
            $finder->files()->in($path)->name($type)->exclude($exclude)->sortByName(); // exclude() only works with directories
        }

        // ability to exclude files with relative path
        foreach ($exclude as $item) {
            $finder->notPath($item);
        }

        return $finder;
    }
}
