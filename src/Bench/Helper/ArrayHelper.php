<?php

namespace Bench\Helper;

/**
 * Class ArrayHelper
 * @package Bench\Helper
 */
class ArrayHelper
{
    /**
     * @param array $headers
     * @return array
     */
    public static function setHeaders(array $headers): array
    {
        foreach ($headers as $index => $header) {
            [$key, $value] = explode(':', $header);
            $headers[trim($key)] = trim($value);
            unset($headers[$index]);
        }

        return $headers;
    }
}
